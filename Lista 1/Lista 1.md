
### Aluno: Joelder Victor Antonino Aguiar
![Lista](./img/escrito.jpg)

### **Questão 3**:  Função f(x)=2sen(x)-3x² 
Gráfico obtido  utilizando o software Geogebra, podemos ver que existem duas raízes uma já é conhecida só em observar a equação, se atribuimos x=0 f(x)=0 a outra se encontra entre o intervalo [0.2,1]


![Gráfico 1.png](./img/grafico1.png)

## Método da Bissecção


```python
import math as mt
def f(x):
    fx=((2*(mt.sin(x)))-(3*(x**2)))
    return fx
# Encontrando a raiz no intervalo de [0.2,1] com precisão de 0.01
a=0.2
b=1
precision=0.01
i=0 
x,fx=[],[]
while(1):
    xi= (a+b) / 2
    x.append(xi)
    fa = f(a)
    fb = f(b)
    fxi = f(xi)
    fx.append(fxi)
    if fa * fxi < 0:
        b= xi
    else:
        a= xi
    if precision > abs(b-a) or precision > abs(f(xi)):
        print('Número de iterações->'+str(i+1))
        print('Valores de x->'+str(x))
        print('Valores de f(x)->'+str(fx))
        break
    i+=1
it3_bissec=x[2]
raiz_bissec=x[4]
```

    Número de iterações->5
    Valores de x->[0.6, 0.8, 0.7, 0.6499999999999999, 0.625]
    Valores de f(x)->[0.04928494679007067, -0.4852878182009548, -0.18156462552461772, -0.05712718852792076, -0.0016804541190755806]


## Método da Falsa Posição


```python
# Encontrando a raiz no intervalo de [0.2,1] com precisão de 0.01
a=0.2
b=1
precision=0.01
i=0 
x,fx=[],[]
while(1):
    xi= (a*f(b)-b*f(a)) /(f(b)-f(a))
    x.append(xi)
    fa = f(a)
    fb = f(b)
    fxi = f(xi)
    fx.append(fxi)
    if fa * fxi < 0:
        b= xi
    else:
        a= xi
    if precision > abs(b-a) or precision > abs(f(xi)):
        print('Número de iterações->'+str(i+1))
        print('Valores de x->'+str(x))
        print('Valores de f(x)->'+str(fx))
        break
    i+=1
it3_falsaP=x[2]
raiz_falsaP=x[6]
```

    Número de iterações->7
    Valores de x->[0.3391566668376342, 0.4684315306017338, 0.5517161900528916, 0.5934293195133503, 0.6116909918266388, 0.6192103761852271, 0.6222278804289278]
    Valores de f(x)->[0.3203021007624256, 0.24469036602259475, 0.13512684206001702, 0.061939542122915636, 0.026007708544709685, 0.010520173379647169, 0.00419127635720673]



```python
# No método da bissecção a iteração de número 3 está distante da raiz em:
delta_bissec= abs(raiz_bissec-it3_bissec)
print('Distância da raiz para terceira iteração-> '+str(delta_bissec))
delta_falsaP= abs(raiz_falsaP-it3_falsaP)
print('Distância da raiz para terceira iteração-> '+str(delta_falsaP))
```

    Distância da raiz para terceira iteração-> 0.07499999999999996
    Distância da raiz para terceira iteração-> 0.07051169037603622


Percebe-se que nesse exemplo a distância difere muito pouco, só ocorre mudança depois da terceira casa decimal.

### **Questão 4:**
#### f(x)=702cos(x) - e^(2x)

Utilizando o software geogebra plotando as funções:

f(x)=702cos(x)

f(x)=e^(2x)

### Fase I

![Grafico 2](./img/grafico2.png)

Observando os gráficos das duas funções se vr que há uma raiz entre o intervalo de [-2,0]

Utilizando o metódo da bissecção:


```python
import math as mt
def f(x):
    fx= ((702*(mt.cos(x))) - (mt.e**(2*x))) 
    return fx

# Encontrando a raiz no intervalo de [-2,0] com precisão de 0.01
a=-2
b=0
precision=0.01
i=0 
x,fx=[],[]
while(1):
    xi= (a+b) / 2
    x.append(xi)
    fa = f(a)
    fb = f(b)
    fxi = f(xi)
    fx.append(fxi)
    if fa * fxi < 0:
        b= xi
    else:
        a= xi
    if  precision > abs(f(xi)): # Observação porque retirei a condição a (a-b) como critério de parada, 
        #porque forçava o programa a parar antes de convergir para zero
        print('Número de iterações->'+str(i+1))
        print('Valores de x->'+str(x))
        print('Valores de f(x)->'+str(fx))
        break
    if i> 23:
        break
    i+=1
```

    Número de iterações->15
    Valores de x->[-1.0, -1.5, -1.75, -1.625, -1.5625, -1.59375, -1.578125, -1.5703125, -1.57421875, -1.572265625, -1.5712890625, -1.57080078125, -1.570556640625, -1.5706787109375, -1.57073974609375]
    Valores de f(x)->[379.1568834361975, 49.60772850235958, -125.15892844936576, -38.07112299674102, 5.780017666325174, -16.15333860224137, -5.187267672970395, 0.29639064216102684, -2.4454555354847094, -1.0745340852611538, -0.3890718041453908, -0.046340560759132635, 0.12502505086944726, 0.03934224695850254, -0.003499156504326209]


### c. Calculando o número de iterações para ɛ=10⁻⁸


```python
e=0.0000001
a=0.5
b=1
k=(mt.log10(b-a)-mt.log10(e))/mt.log10(2)
print(k)

```

    22.253496664211536


Como o número de iterações deve ser maior do que o encontrado ,será necessário 23 iterações.


